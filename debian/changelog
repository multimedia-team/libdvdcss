libdvdcss (1.4.3-1) unstable; urgency=medium

  * Team upload.

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Unit 193 ]
  * d/u/signing-key.asc, d/watch: Add upstream's signing key and check releases
  * New upstream version 1.4.3.
  * d/control:
    - Bump dh compat to 13.
    - R³: no.
  * d/libdvdcss2.install: Install AUTHORS and README.
  * d/libdvdcss2.symbols: Add Build-Depends-Package.
  * d/not-installed: Ignore ChangeLog, COPYING, and NEWS as they're duplicates.
  * d/rules:
    - Exclude *.la from dh_missing.
    - Drop --as-needed from LDFLAGS, default now.
  * Update Standards-Version to 4.6.0.

 -- Unit 193 <unit193@debian.org>  Fri, 17 Dec 2021 05:51:41 -0500

libdvdcss (1.4.2-1) unstable; urgency=medium

  * New upstream version 1.4.2

 -- Fabian Greffrath <fabian@debian.org>  Mon, 26 Mar 2018 21:30:42 +0200

libdvdcss (1.4.1-1) unstable; urgency=medium

  * New upstream version 1.4.1.
  * Update debian/watch file.
  * Enable all hardening flags.
  * Bump debhelper compat to 10.
  * Bump Standards-Version to 4.1.2.

 -- Fabian Greffrath <fabian@debian.org>  Mon, 08 Jan 2018 21:22:12 +0100

libdvdcss (1.4.0-1) unstable; urgency=medium

  [ Rico Tzschichholz ]
  * Imported Upstream version 1.4.0

  [ Fabian Greffrath ]
  * Update Uploaders and Debian packaging copyright information.

 -- Fabian Greffrath <fabian@debian.org>  Mon, 08 Feb 2016 21:47:16 +0100

libdvdcss (1.3.99-1) unstable; urgency=medium

  * Imported Upstream version 1.3.99
  * Bump Build-Depends to "debhelper (>= 9)",
    remove debian/source/lintian-overrides;
    remove debian/source/local-options, they are default now.
  * Fix unversioned-copyright-format-uri lintian warning.
  * Add dvdcss_open_stream@Base symbol to debian/libdvdcss2.symbols file.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Mon, 11 May 2015 15:57:33 +0200

libdvdcss (1.3.0-1) unstable; urgency=medium

  * Imported Upstream version 1.3.0
  * debian/libdvdcss2.symbols: adapt to removed API.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Tue, 05 Aug 2014 10:50:49 +0200

libdvdcss (1.2.13-1) unstable; urgency=low

  [ Andres Mejia ]
  * Reduce build dependencies. Remove need for dh-autoreconf.
  * Drop debhelper build dependency down to (>= 8.1.3~).
  * Add myself to Uploaders field.
  * Bump to Standards-Version 3.9.3.
  * Make dev package multiarch installable installable.
  * Ignore package-needs-versioned-debhelper-build-depends 9 lintian warning.
  * Support parallel builds.
  * Set -fvisibility=hidden through CFLAGS defined in debian/rules.

  [ Fabian Greffrath ]
  * Imported Upstream version 1.2.13
  * Remove patches applied upstream.
  * API documentation is not built without doxygen.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Wed, 27 Feb 2013 13:14:33 +0100

libdvdcss (1.2.12-1) unstable; urgency=low

  * Imported Upstream version 1.2.12
  * Remove patch applied upstream.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Mon, 12 Mar 2012 11:11:59 +0100

libdvdcss (1.2.11-4) unstable; urgency=low

  * If unsure, assume the drive is of RPC-I type.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Mon, 20 Feb 2012 10:03:29 +0100

libdvdcss (1.2.11-3) unstable; urgency=low

  * Set appropriate symbol visibility attributes.
  * Simplify libdvdcss2.symbols file accordingly.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Thu, 26 Jan 2012 09:45:36 +0100

libdvdcss (1.2.11-2) unstable; urgency=low

  * Further clean up debian/copyright.
  * Multi-Archify.
  * Add back static library, it's built anyway.
  * Add back libdvdcss2.symbols file, but ignore internal symbols.
  * Bump Build-Depends to debhelper (>= 9).

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Wed, 25 Jan 2012 16:01:42 +0100

libdvdcss (1.2.11-1) unstable; urgency=low

  * Imported Upstream version 1.2.11
  * Clean up debian/copyright.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Tue, 13 Dec 2011 13:29:17 +0100

libdvdcss (1.2.10-2) unstable; urgency=low

  [ Fabian Greffrath ]
  * Drop the symbols file, it exposed too much API and libdvdcss2
    is usually dlopen()ed anyway; add a lintian overrides.
  * Adjust license for the Debian packaging to GPL-2+.

  [ Alessio Treglia ]
  * Drop -DBG package.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Thu, 28 Jul 2011 13:19:36 +0200

libdvdcss (1.2.10-1) unstable; urgency=low

  * Initial release (Closes: #154281).

 -- Alessio Treglia <alessio@debian.org>  Thu, 28 Jul 2011 11:26:05 +0200
